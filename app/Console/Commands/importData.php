<?php

namespace App\Console\Commands;

use App\Models\Movie;
use App\Service\MovieService;
use Illuminate\Console\Command;

class importData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load Data from API and store it in database';

    /**
     * Execute the console command.
     */
    public function __construct(private readonly MovieService $movieService)
    {
        parent::__construct();
    }

    public function handle()
    {
        $movies = $this->movieService->getAll();
        foreach ($movies->results as $movie) {
            if (Movie::where('IdMovie', $movie->id)->first()) {
                continue;
            } else {
                Movie::create([
                    'IdMovie' => $movie->id,
                    'title' => $movie->title ?? null,
                    'original_language' => $movie->original_language,
                    'original_title' => $movie->original_title ?? null,
                    'overview' => $movie->overview,
                    'poster_path' => $movie->poster_path,
                    'media_type' => $movie->media_type,
                    'release_date' => $movie->release_date ?? null,
                    'popularity' => $movie->popularity,
                    'vote_average' => $movie->vote_average,
                    'vote_count' => $movie->vote_count
                ]);
            }
        }
        $this->info("Data imported Succesfully!");
    }
}
