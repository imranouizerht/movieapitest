<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $string, $id)
 * @method static paginate(int $int)
 */
class Movie extends Model
{
    use HasFactory;

    protected  $fillable = [
        'Id',
        'IdMovie',
        'title',
        'original_language',
        'original_title',
        'overview',
        'poster_path',
        'media_type',
        'popularity',
        'release_date',
        'vote_average',
        'vote_count'
    ];
}
