<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Service\MovieService;
use Illuminate\View\View;

class MovieController extends Controller
{
    public function __construct(private readonly MovieService $movieService)
    {
    }

    public function getMovies(): View
    {
        $movies = Movie::paginate(5);
        return view('Movies/movies', ['movies' => $movies]);

    }

    public function getMovie(int $movieId): View
    {
        //$movie = DB::select('SELECT * from movies where ID="'.$movieId .'"');
        $movie = $this->movieService->getOne($movieId);
        return view('Movies/movie_details', ['movie' => $movie]);
    }
}
