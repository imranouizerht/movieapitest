<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class MovieService
{
    public function __construct(private readonly Client $client)
    {
    }

    public function getAll()
    {
        try {
            $response = $this->client->request('GET', env('MOVIE_API_ALL'), [
                'headers' => [
                    "Authorization" => "Bearer " . env('MOVIE_API_TOKEN')
                ]
            ]);
            return json_decode($response->getBody());

        } catch (GuzzleException $exception) {
            return $exception->getMessage();
        }

    }

    public function getOne(int $movieId)
    {
        try {
            $response = $this->client->request('GET', env('MOVIE_API_ONE') . '/' . $movieId, [
                'headers' => [
                    "Authorization" => "Bearer " . env('MOVIE_API_TOKEN')
                ]
            ]);
            return json_decode($response->getBody());
        } catch (GuzzleException $exception) {
            return $exception->getMessage();
        }

    }

}
