<?php

namespace Tests\Unit;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    private Client $http;

    public function setUp(): void
    {
        $this->http = new Client();
    }

    public function test_api_is_responding_and_where_getting_data(): void
    {

        try {
            $response = $this->http->request('GET', env('MOVIE_API_ALL'), [
                'headers' => ['Authorization' => 'Bearer ' . env('MOVIE_API_TOKEN')]
            ]);
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertJson($response->getBody());
        } catch (GuzzleException $e) {
            echo $e->getMessage();
        }

    }

    public function test_api_is_getting_single_movie_from_api(): void
    {
        try {
            $response = $this->http->request('GET', env('MOVIE_API_ONE') . '/' . '964980', [
                'headers' => ['Authorization' => 'Bearer ' . env('MOVIE_API_TOKEN')]
            ]);
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertJson($response->getBody());
            $movie = json_decode($response->getBody(), true);
            $this->assertArrayHasKey('overview', $movie);
        } catch (GuzzleException $e) {
            echo $e->getMessage();
        }
    }


    public function tearDown(): void
    {
    }
}
