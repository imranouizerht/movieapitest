<?php

namespace Tests\Unit;

use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public function test_registration_and_redirection_after_it()
    {
        $response = $this->post('/register', [
            'name' => 'Imran',
            'email' => 'imran@gmail.com',
            'password' => 'password',
            'password_confimation' => 'password'
        ]);
        $response->assertRedirect('/');
    }
}
