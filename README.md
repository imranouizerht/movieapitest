# Movie API TEST

## Requirements:

- Composer
- Docker

## Getting Started

To make the project start follow the following steps :

- clone the repository of project    
  `git clone git@gitlab.com:imranouizerht/movieapitest.git`
  If you're using linux
  `cp .env.example .env`
- If you're in windows duplicate the copy the .env.example on a new file called .env
  Next run:
  `composer install`
  `docker-compose up -d`
  `docker exec -it laravel-tuto-laravel.test-1 bash`
  `composer install`
  `npm install`
  `npm run build`
  `php artisan migrate`
  to load the api data on the database you can use :
  `php artisan import:data`

- And now visite the website on localhost port 80:
  we have authentificationn so make sure to visite
  '/register' to create your account then go to '/login'and checkout the website
  -'/movies' to list the latest top movies
  and choose movie from the list to look to the details.

Thanks for following those steps !

## Documentation:

- https://laravel.com/docs/10.x/sail
- https://jetstream.laravel.com/3.x/introduction.html
