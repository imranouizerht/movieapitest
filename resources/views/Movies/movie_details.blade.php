<style>
    img {
        padding-left: 500px;
    }

    h3 {
        text-align: center;

    }
</style>
<x-app-layout>
    <h2>{{$movie->title ?? ''}}</h2>
    <img src="https://image.tmdb.org/t/p/w500/{{$movie->backdrop_path ?? $movie->poster_path ?? ''}}"
         alt="{{$movie->title ?? ''}}">
    <h3>Quote : <em><strong>"{{$movie->tagline ?? 'Unknown'}}"</strong></em></h3>

    <br>

    <h3><strong>Categorie:</strong>
        <ul>
            @foreach($movie->genres as $genre)
                <li>{{$genre->name}}</li>
            @endforeach
        </ul>

    </h3>
    <h3><strong>Orignal language:</strong><br> {{$movie->original_language}}</h3>
    <h3><strong>Overview :</strong> <br>{{$movie->overview}} </h3>
    <h3><strong>Popularity:<br></strong> {{$movie->popularity}}</h3>
    <h3><strong>Production:</strong>
        <ul>
            @foreach($movie->production_companies as $production)
                <li>{{$production->name}}</li>
            @endforeach
        </ul>
    </h3>
    <h3><strong>Released :</strong><br> {{$movie->release_date ?? 'Soon'}}</h3>>
    <h3>
        <strong>Language Availble:</strong>
        <ul>
            @foreach($movie->spoken_languages as $language)
                <li>{{$language->english_name}}</li>
            @endforeach
        </ul>
    </h3>
</x-app-layout>
