<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Top Movies') }}
        </h2>
    </x-slot>
<div class="flex justify-around flex-wrap min-h-screen bg-gray-100 py-6">
    @foreach($movies as $movie)
        <div class=" flex flex-col justify-center sm:py-12">

            <div class="py-3 sm:max-w-xl sm:mx-auto">
                <a href="/movies/{{$movie->IdMovie}}">
                    <div class="bg-white shadow-lg border-gray-100 max-h-80	 border sm:rounded-3xl p-8 flex space-x-8">
                        <div class="h-48 overflow-visible w-1/2">
                            <img class="rounded-3xl shadow-lg" src="https://image.tmdb.org/t/p/w500/{{$movie->poster_path}}" alt="{{$movie->title ?? $movie->title ?? ''}}">
                        </div>
                        <div class="flex flex-col w-1/2 space-y-4">
                            <div class="flex justify-between items-start">
                                <h2 class="text-3xl font-bold">{{$movie->title ?? $movie->original_title ?? ''}}</h2>
                                <div class="bg-yellow-400 font-bold rounded-xl p-2">{{$movie->vote_average}}</div>
                            </div>
                            <div>
                                <div class="text-sm text-gray-400">{{$movie->media_type}}</div>
                                <div class="text-lg text-gray-800">{{$movie->release_date ?? 'Soon'}}</div>
                            </div>
                            <p class=" text-gray-400 max-h-40 overflow-y-scroll">{{$movie->overview}}</p>
                        </div>

                    </div>
                </a>
            </div>

        </div>
    @endforeach
</div>
    {!! $movies->withQueryString()->links('pagination::bootstrap-5') !!}
</x-app-layout>
